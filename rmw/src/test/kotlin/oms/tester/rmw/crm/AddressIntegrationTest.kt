package oms.tester.rmw.crm

import junit.framework.TestCase.fail
import oms.tester.utility.EndpointUtilities
import oms.tester.utility.MessageUtilities
import oms.tester.utility.RequestSender
import oms.tester.utility.TestContextProvider
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.web.reactive.function.client.WebClient
import java.util.UUID

@RunWith(SpringRunner::class)
@ContextConfiguration(classes = [TestContextProvider::class])
@WebFluxTest
class AddressIntegrationTest {

    private val requestSender = RequestSender(WebClient.builder().build())

    private lateinit var customerId: String
    private lateinit var authenticationToken: String

    @Before
    fun createActiveTestCustomer() {
        val user = mapOf(
            "username" to "${UUID.randomUUID()}@test.com",
            "password" to "MySuperSecurePassword12345"
        )
        val userId = requestSender
            .post(EndpointUtilities.RMW.Auth.NEW_USER_ENDPOINT, user)
            .map { response -> response.body?.findPath("data")?.textValue() ?: "" }
            .get()
        requestSender.get(EndpointUtilities.RMW.Auth.activateUserEndpoint(userId))
        this.authenticationToken = requestSender
            .post(EndpointUtilities.RMW.Auth.AUTHENTICATE_USER_ENDPOINT, user)
            .map { response -> response.body?.findPath("data")?.textValue() ?: "" }
            .get()
        this.customerId = requestSender.post(EndpointUtilities.RMW.CRM.Customer.NEW_CUSTOMER_ENDPOINT, mapOf(
            "userId" to userId,
            "forename" to "Derp",
            "surname" to "Derpington",
            "contactNumber" to "07123456789"))
            .map { response -> response.body?.findPath("data")?.textValue() ?: "" }
            .get()
    }

    @Test
    fun `Creating an address should succeed`() {
        requestSender.post(EndpointUtilities.RMW.CRM.Address.NEW_ADDRESS_ENDPOINT, mapOf(
            "customerId" to this.customerId,
            "street1" to "54 Derp Road",
            "city" to "DerpTown",
            "county" to "DerpCounty",
            "postcode1" to "AB12",
            "postcode2" to "3CD"
        ), this.authenticationToken).map { response ->
            when {
                response.statusCode != HttpStatus.CREATED ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.CREATED, response.statusCode))
                response.body == null ->
                    fail(MessageUtilities.ERROR_EXPECTED_BODY)
            }
        }
    }

    @Test
    fun `Creating an address should reject invalid requests`() {
        requestSender.post(EndpointUtilities.RMW.CRM.Address.NEW_ADDRESS_ENDPOINT, mapOf(
            "street1" to "54 Derp Road",
            "county" to "",
            "postcode2" to "1231231231231"
        ), this.authenticationToken).map { response ->
            when {
                response.statusCode != HttpStatus.BAD_REQUEST ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.BAD_REQUEST, response.statusCode))
                response.body == null ->
                    fail(MessageUtilities.ERROR_EXPECTED_BODY)
                else -> {
                    val faultMessages = response.body!!.findPath("data").toList()
                        .map { node -> node.findValue("message").textValue() }
                    when {
                        !faultMessages.contains("The mandatory customer ID field was expected but was not provided.") ->
                            fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                        !faultMessages.contains("The mandatory city field was expected but was not provided.") ->
                            fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                        !faultMessages.contains("The mandatory county field was expected but was not provided.") ->
                            fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                        !faultMessages.contains("The mandatory postcode field was expected but was not provided.") ->
                            fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                        !faultMessages.contains("The postcode field exceeded the maximum length of 4.") ->
                            fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                    }
                }
            }
        }
    }

    @Test
    fun `Creating an address should reject unauthorised entities`() {
        requestSender.post(EndpointUtilities.RMW.CRM.Address.NEW_ADDRESS_ENDPOINT, mapOf()).map { response ->
            when {
                response.statusCode != HttpStatus.UNAUTHORIZED ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.UNAUTHORIZED, response.statusCode))
                response.body == null ->
                    fail(MessageUtilities.ERROR_EXPECTED_BODY)
                response.body?.findPath("data")?.textValue() != "The request did not contain a valid authentication token" ->
                    fail(MessageUtilities.ERROR_MISSING_MESSAGES)
            }
        }
    }

    @Test
    fun `Updating an address should succeed`() {
        val addressId: String = requestSender.post(EndpointUtilities.RMW.CRM.Address.NEW_ADDRESS_ENDPOINT, mapOf(
            "customerId" to this.customerId,
            "street1" to "54 Derp Road",
            "city" to "DerpTown",
            "county" to "DerpCounty",
            "postcode1" to "AB12",
            "postcode2" to "3CD"
        ), this.authenticationToken).map { response -> response.body?.findPath("data")?.textValue() ?: "" }.get()
        requestSender.put(EndpointUtilities.RMW.CRM.Address.updateAddressEndpoint(addressId), mapOf(
            "street1" to "Some Street",
            "street2" to "Some Street 2"
        ), this.authenticationToken).map { response ->
            when {
                response.statusCode != HttpStatus.OK ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.OK, response.statusCode))
                response.body != null ->
                    fail(MessageUtilities.ERROR_UNEXPECTED_BODY)
            }
        }
    }

    @Test
    fun `Updating an address should reject invalid requests`() {
        val addressId: String = requestSender.post(EndpointUtilities.RMW.CRM.Address.NEW_ADDRESS_ENDPOINT, mapOf(
            "customerId" to this.customerId,
            "street1" to "54 Derp Road",
            "city" to "DerpTown",
            "county" to "DerpCounty",
            "postcode1" to "AB12",
            "postcode2" to "3CD"
        ), this.authenticationToken).map { response -> response.body?.findPath("data")?.textValue() ?: "" }.get()
        requestSender.put(EndpointUtilities.RMW.CRM.Address.updateAddressEndpoint(addressId), mapOf(
            "postcode1" to "123453345435",
            "street1" to "%&*(&*%("
        ), this.authenticationToken).map { response ->
            when {
                response.statusCode != HttpStatus.BAD_REQUEST ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.BAD_REQUEST, response.statusCode))
                response.body == null ->
                    fail(MessageUtilities.ERROR_EXPECTED_BODY)
                else -> {
                    val faultMessages = response.body!!.findPath("data").toList()
                        .map { node -> node.findValue("message").textValue() }
                    when {
                        !faultMessages.contains("The street1 field contained invalid characters.") ->
                            fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                        !faultMessages.contains("The postcode field exceeded the maximum length of 4.") ->
                            fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                    }
                }
            }
        }
    }

    @Test
    fun `Updating an address should reject unauthorised entities`() {
        requestSender.put(EndpointUtilities.RMW.CRM.Address.updateAddressEndpoint("12345"), mapOf()).map { response ->
            when {
                response.statusCode != HttpStatus.UNAUTHORIZED ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.UNAUTHORIZED, response.statusCode))
                response.body == null ->
                    fail(MessageUtilities.ERROR_EXPECTED_BODY)
                response.body?.findPath("data")?.textValue() != "The request did not contain a valid authentication token" ->
                    fail(MessageUtilities.ERROR_MISSING_MESSAGES)
            }
        }
    }

    @Test
    fun `Updating an address should return a 404 response when a customer cannot be found`() {
        requestSender.put(EndpointUtilities.RMW.CRM.Address.updateAddressEndpoint("12345"), mapOf(), this.authenticationToken).map { response ->
            when {
                response.statusCode != HttpStatus.NOT_FOUND ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.NOT_FOUND, response.statusCode))
                response.body != null ->
                    fail(MessageUtilities.ERROR_UNEXPECTED_BODY)
            }
        }
    }

    @Test
    fun `Removing an address should succeed`() {
        val addressId: String = requestSender.post(EndpointUtilities.RMW.CRM.Address.NEW_ADDRESS_ENDPOINT, mapOf(
            "customerId" to this.customerId,
            "street1" to "54 Derp Road",
            "city" to "DerpTown",
            "county" to "DerpCounty",
            "postcode1" to "AB12",
            "postcode2" to "3CD"
        ), this.authenticationToken).map { response -> response.body?.findPath("data")?.textValue() ?: "" }.get()
        requestSender.delete(EndpointUtilities.RMW.CRM.Address.deleteAddressEndpoint(addressId), this.authenticationToken).map { response ->
            when {
                response.statusCode != HttpStatus.OK ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.OK, response.statusCode))
                response.body != null ->
                    fail(MessageUtilities.ERROR_UNEXPECTED_BODY)
            }
        }
    }

    @Test
    fun `Removing an address should reject unauthorised entities`() {
        requestSender.delete(EndpointUtilities.RMW.CRM.Address.deleteAddressEndpoint("12345")).map { response ->
            when {
                response.statusCode != HttpStatus.UNAUTHORIZED ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.UNAUTHORIZED, response.statusCode))
                response.body == null ->
                    fail(MessageUtilities.ERROR_EXPECTED_BODY)
                response.body?.findPath("data")?.textValue() != "The request did not contain a valid authentication token" ->
                    fail(MessageUtilities.ERROR_MISSING_MESSAGES)
            }
        }
    }

    @Test
    fun `Removing an address should return a 404 response when a customer cannot be found`() {
        requestSender.delete(EndpointUtilities.RMW.CRM.Address.deleteAddressEndpoint("12345"), this.authenticationToken).map { response ->
            when {
                response.statusCode != HttpStatus.NOT_FOUND ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.NOT_FOUND, response.statusCode))
                response.body != null ->
                    fail(MessageUtilities.ERROR_UNEXPECTED_BODY)
            }
        }
    }

    @Test
    fun `Viewing an address should succeed`() {
        val addressId: String = requestSender.post(EndpointUtilities.RMW.CRM.Address.NEW_ADDRESS_ENDPOINT, mapOf(
            "customerId" to this.customerId,
            "street1" to "54 Derp Road",
            "city" to "DerpTown",
            "county" to "DerpCounty",
            "postcode1" to "AB12",
            "postcode2" to "3CD"
        ), this.authenticationToken).map { response -> response.body?.findPath("data")?.textValue() ?: "" }.get()
        requestSender.get(EndpointUtilities.RMW.CRM.Address.viewAddressEndpoint(addressId), this.authenticationToken).map { response ->
            when {
                response.statusCode != HttpStatus.OK ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.OK, response.statusCode))
                response.body == null ->
                    fail(MessageUtilities.ERROR_EXPECTED_BODY)
                response.body?.findPath("street1")?.textValue() != "54 Derp Road" ->
                    fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                response.body?.findPath("city")?.textValue() != "DerpTown" ->
                    fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                response.body?.findPath("county")?.textValue() != "DerpCounty" ->
                    fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                response.body?.findPath("postcode1")?.textValue() != "AB12" ->
                    fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                response.body?.findPath("postcode2")?.textValue() != "3CD" ->
                    fail(MessageUtilities.ERROR_MISSING_MESSAGES)
            }
        }
    }

    @Test
    fun `Viewing an address should reject unauthorised entities`() {
        requestSender.get(EndpointUtilities.RMW.CRM.Address.viewAddressEndpoint("12345")).map { response ->
            when {
                response.statusCode != HttpStatus.UNAUTHORIZED ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.UNAUTHORIZED, response.statusCode))
                response.body == null ->
                    fail(MessageUtilities.ERROR_EXPECTED_BODY)
                response.body?.findPath("data")?.textValue() != "The request did not contain a valid authentication token" ->
                    fail(MessageUtilities.ERROR_MISSING_MESSAGES)
            }
        }
    }

    @Test
    fun `Viewing an address should return a 404 response when a customer cannot be found`() {
        requestSender.get(EndpointUtilities.RMW.CRM.Address.viewAddressEndpoint("12345"), this.authenticationToken).map { response ->
            when {
                response.statusCode != HttpStatus.NOT_FOUND ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.NOT_FOUND, response.statusCode))
                response.body != null ->
                    fail(MessageUtilities.ERROR_UNEXPECTED_BODY)
            }
        }
    }
}