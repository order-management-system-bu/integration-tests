package oms.tester.rmw.crm

import junit.framework.TestCase
import junit.framework.TestCase.fail
import oms.tester.utility.EndpointUtilities
import oms.tester.utility.MessageUtilities
import oms.tester.utility.RequestSender
import oms.tester.utility.TestContextProvider
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.web.reactive.function.client.WebClient
import java.util.UUID

@RunWith(SpringRunner::class)
@ContextConfiguration(classes = [TestContextProvider::class])
@WebFluxTest
class CustomerIntegrationTest {

    private val requestSender = RequestSender(WebClient.builder().build())

    private lateinit var username: String
    private lateinit var userId: String
    private lateinit var authenticationToken: String

    @Before
    fun createActiveTestUser() {
        this.username = "${UUID.randomUUID()}@test.com"
        val user = mapOf(
            "username" to this.username,
            "password" to "MySuperSecurePassword12345"
        )
        this.userId = requestSender
            .post(EndpointUtilities.RMW.Auth.NEW_USER_ENDPOINT, user)
            .map { response -> response.body?.findPath("data")?.textValue() ?: "" }
            .get()
        requestSender.get(EndpointUtilities.RMW.Auth.activateUserEndpoint(this.userId))
        this.authenticationToken = requestSender
            .post(EndpointUtilities.RMW.Auth.AUTHENTICATE_USER_ENDPOINT, user)
            .map { response -> response.body?.findPath("data")?.textValue() ?: "" }
            .get()
    }

    @Test
    fun `Creating a customer should succeed`() {
        requestSender.post(EndpointUtilities.RMW.CRM.Customer.NEW_CUSTOMER_ENDPOINT, mapOf(
            "userId" to this.userId,
            "forename" to "Derp",
            "surname" to "Derpington",
            "contactNumber" to "07123456789"
        )).map { response ->
            when {
                response.statusCode != HttpStatus.CREATED ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.CREATED, response.statusCode))
                response.body == null ->
                    fail(MessageUtilities.ERROR_EXPECTED_BODY)
            }
        }
    }

    @Test
    fun `Creating a customer should reject invalid requests`() {
        requestSender.post(EndpointUtilities.RMW.CRM.Customer.NEW_CUSTOMER_ENDPOINT, mapOf(
            "forename" to "Derp",
            "contactNumber" to "123"
        )).map { response ->
            when {
                response.statusCode != HttpStatus.BAD_REQUEST ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.BAD_REQUEST, response.statusCode))
                response.body == null ->
                    fail(MessageUtilities.ERROR_EXPECTED_BODY)
                else -> {
                    val faultMessages = response.body!!.findPath("data").toList()
                        .map { node -> node.findValue("message").textValue() }
                    when {
                        !faultMessages.contains("The mandatory user ID field was expected but was not provided.") ->
                            TestCase.fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                        !faultMessages.contains("The mandatory surname field was expected but was not provided.") ->
                            TestCase.fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                        !faultMessages.contains("The provided contact number was not a valid mobile number.") ->
                            TestCase.fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                    }
                }
            }
        }
    }

    @Test
    fun `Creating a customer should reject existing users`() {
        val customer = mapOf(
            "userId" to this.userId,
            "forename" to "Derp",
            "surname" to "Derpington",
            "contactNumber" to "07123456789"
        )
        requestSender.post(EndpointUtilities.RMW.CRM.Customer.NEW_CUSTOMER_ENDPOINT, customer)
        requestSender.post(EndpointUtilities.RMW.CRM.Customer.NEW_CUSTOMER_ENDPOINT, customer)
            .map { response ->
                when {
                    response.statusCode != HttpStatus.BAD_REQUEST ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.BAD_REQUEST, response.statusCode))
                    response.body == null ->
                        fail(MessageUtilities.ERROR_EXPECTED_BODY)
                    response.body!!.findPath("data").textValue() != "An existing customer is already associated with the provided email address." ->
                        fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                }
            }
    }

    @Test
    fun `Updating a customer should succeed`() {
        val customerId: String = requestSender.post(EndpointUtilities.RMW.CRM.Customer.NEW_CUSTOMER_ENDPOINT, mapOf(
            "userId" to this.userId,
            "forename" to "Derp",
            "surname" to "Derpington",
            "contactNumber" to "07123456789"
        )).map { response -> response.body?.findPath("data")?.textValue() ?: "" }.get()
        requestSender.put(EndpointUtilities.RMW.CRM.Customer.updateCustomerEndpoint(customerId), mapOf(
            "forename" to "Liam",
            "surname" to "McGuire"
        ), authenticationToken).map { response ->
            when {
                response.statusCode != HttpStatus.OK ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.OK, response.statusCode))
                response.body != null ->
                    fail(MessageUtilities.ERROR_UNEXPECTED_BODY)
            }
        }
    }

    @Test
    fun `Updating a customer should reject invalid requests`() {
        requestSender.post(EndpointUtilities.RMW.CRM.Customer.NEW_CUSTOMER_ENDPOINT, mapOf(
            "userId" to this.userId,
            "forename" to "Derp",
            "surname" to "Derpington",
            "contactNumber" to "07123456789"
        ))
        requestSender.put(EndpointUtilities.RMW.CRM.Customer.updateCustomerEndpoint(this.userId), mapOf(
            "forename" to "£$^&$^*",
            "surname" to "&*&(*&(*%$&(*&"
        ), authenticationToken).map { response ->
            when {
                response.statusCode != HttpStatus.BAD_REQUEST ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.BAD_REQUEST, response.statusCode))
                response.body == null ->
                    fail(MessageUtilities.ERROR_EXPECTED_BODY)
                else -> {
                    val faultMessages = response.body!!.findPath("data").toList()
                        .map { node -> node.findValue("message").textValue() }
                    when {
                        !faultMessages.contains("The forename field contained invalid characters.") ->
                            TestCase.fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                        !faultMessages.contains("The surname field contained invalid characters.") ->
                            TestCase.fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                    }
                }
            }
        }
    }

    @Test
    fun `Updating a customer should reject unauthorised entities`() {
        requestSender.put(EndpointUtilities.RMW.CRM.Customer.updateCustomerEndpoint(this.userId), mapOf(
            "forename" to "Liam",
            "surname" to "McGuire"
        )).map { response ->
            when {
                response.statusCode != HttpStatus.UNAUTHORIZED ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.UNAUTHORIZED, response.statusCode))
                response.body == null ->
                    fail(MessageUtilities.ERROR_EXPECTED_BODY)
                response.body?.findPath("data")?.textValue() != "The request did not contain a valid authentication token" ->
                    fail(MessageUtilities.ERROR_MISSING_MESSAGES)
            }
        }
    }

    @Test
    fun `Updating a customer should return a a 404 response when a customer cannot be found`() {
        requestSender.put(EndpointUtilities.RMW.CRM.Customer.updateCustomerEndpoint("12345"), mapOf(
            "forename" to "Liam",
            "surname" to "McGuire"
        ), this.authenticationToken).map { response ->
            when {
                response.statusCode != HttpStatus.NOT_FOUND ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.NOT_FOUND, response.statusCode))
                response.body != null ->
                    fail(MessageUtilities.ERROR_UNEXPECTED_BODY)
            }
        }
    }

    @Test
    fun `Removing a customer should succeed`() {
        val customerId: String = requestSender.post(EndpointUtilities.RMW.CRM.Customer.NEW_CUSTOMER_ENDPOINT, mapOf(
            "userId" to this.userId,
            "forename" to "Derp",
            "surname" to "Derpington",
            "contactNumber" to "07123456789"
        )).map { response -> response.body?.findPath("data")?.textValue() ?: "" }.get()
        requestSender.delete(EndpointUtilities.RMW.CRM.Customer.deleteCustomerEndpoint(customerId), this.authenticationToken)
            .map { response ->
                when {
                    response.statusCode != HttpStatus.OK ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.OK, response.statusCode))
                    response.body != null ->
                        fail(MessageUtilities.ERROR_UNEXPECTED_BODY)
                }
            }
    }

    @Test
    fun `Removing a customer should reject unauthorised entities`() {
        requestSender.delete(EndpointUtilities.RMW.CRM.Customer.deleteCustomerEndpoint("12345"))
            .map { response ->
                when {
                    response.statusCode != HttpStatus.UNAUTHORIZED ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.UNAUTHORIZED, response.statusCode))
                    response.body == null ->
                        fail(MessageUtilities.ERROR_EXPECTED_BODY)
                    response.body?.findPath("data")?.textValue() != "The request did not contain a valid authentication token" ->
                        fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                }
            }
    }

    @Test
    fun `Removing a customer should return a 404 response when a customer cannot be found`() {
        requestSender.delete(EndpointUtilities.RMW.CRM.Customer.deleteCustomerEndpoint("12345"), this.authenticationToken)
            .map { response ->
                when {
                    response.statusCode != HttpStatus.NOT_FOUND ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.NOT_FOUND, response.statusCode))
                    response.body != null ->
                        fail(MessageUtilities.ERROR_UNEXPECTED_BODY)
                }
            }
    }

    @Test
    fun `Viewing a customer should succeed`() {
        val customerId: String = requestSender.post(EndpointUtilities.RMW.CRM.Customer.NEW_CUSTOMER_ENDPOINT, mapOf(
            "userId" to this.userId,
            "forename" to "Derp",
            "surname" to "Derpington",
            "contactNumber" to "07123456789"
        )).map { response -> response.body?.findPath("data")?.textValue() ?: "" }.get()
        requestSender.get(EndpointUtilities.RMW.CRM.Customer.viewCustomerEndpoint(customerId), this.authenticationToken)
            .map { response ->
                when {
                    response.statusCode != HttpStatus.OK ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.OK, response.statusCode))
                    response.body == null ->
                        fail(MessageUtilities.ERROR_EXPECTED_BODY)
                    response.body?.findPath("forename")?.textValue() != "Derp" ->
                        fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                    response.body?.findPath("surname")?.textValue() != "Derpington" ->
                        fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                    response.body?.findPath("contactNumber")?.textValue() != "07123456789" ->
                        fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                }
            }
    }

    @Test
    fun `Viewing a customer should reject unauthorised entities`() {
        requestSender.get(EndpointUtilities.RMW.CRM.Customer.viewCustomerEndpoint("12345"))
            .map { response ->
                when {
                    response.statusCode != HttpStatus.UNAUTHORIZED ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.UNAUTHORIZED, response.statusCode))
                    response.body == null ->
                        fail(MessageUtilities.ERROR_EXPECTED_BODY)
                    response.body?.findPath("data")?.textValue() != "The request did not contain a valid authentication token" ->
                        fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                }
            }
    }

    @Test
    fun `Viewing a customer should return a 404 response when a customer cannot be found`() {
        requestSender.get(EndpointUtilities.RMW.CRM.Customer.viewCustomerEndpoint("12345"), this.authenticationToken)
            .map { response ->
                when {
                    response.statusCode != HttpStatus.NOT_FOUND ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.NOT_FOUND, response.statusCode))
                    response.body != null ->
                        fail(MessageUtilities.ERROR_UNEXPECTED_BODY)
                }
            }
    }
}