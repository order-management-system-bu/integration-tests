package oms.tester.rmw.auth

import junit.framework.TestCase.fail
import oms.tester.utility.EndpointUtilities
import oms.tester.utility.MessageUtilities
import oms.tester.utility.RequestSender
import oms.tester.utility.TestContextProvider
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.web.reactive.function.client.WebClient
import java.util.UUID

@RunWith(SpringRunner::class)
@ContextConfiguration(classes = [TestContextProvider::class])
@WebFluxTest
class ActivationsIntegrationTest {

    private val requestSender = RequestSender(WebClient.builder().build())

    private lateinit var userId: String

    @Before
    fun createActiveTestUser() {
        val user = mapOf(
            "username" to "${UUID.randomUUID()}@test.com",
            "password" to "MySuperSecurePassword12345"
        )
        this.userId = requestSender
            .post(EndpointUtilities.RMW.Auth.NEW_USER_ENDPOINT, user)
            .map { response -> response.body?.findPath("data")?.textValue() ?: "" }
            .get()
    }

    @Test
    fun `Activating a user should succeed`() {
        requestSender.get(EndpointUtilities.RMW.Auth.activateUserEndpoint(this.userId))
            .map { response ->
                when {
                    response.statusCode != HttpStatus.OK ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.OK, response.statusCode))
                    response.body != null ->
                        fail(MessageUtilities.ERROR_UNEXPECTED_BODY)
                }
            }
    }

    @Test
    fun `Activating a user return a 404 response when a user is not found`() {
        requestSender.get(EndpointUtilities.RMW.Auth.activateUserEndpoint("12345"))
            .map { response ->
                when {
                    response.statusCode != HttpStatus.NOT_FOUND ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.NOT_FOUND, response.statusCode))
                    response.body != null ->
                        fail(MessageUtilities.ERROR_UNEXPECTED_BODY)
                }
            }
    }

}