package oms.tester.rmw.auth

import junit.framework.TestCase.fail
import oms.tester.utility.EndpointUtilities
import oms.tester.utility.MessageUtilities
import oms.tester.utility.RequestSender
import oms.tester.utility.TestContextProvider
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.web.reactive.function.client.WebClient
import java.util.UUID

@RunWith(SpringRunner::class)
@ContextConfiguration(classes = [TestContextProvider::class])
@WebFluxTest
class AuthenticationIntegrationTest {

    private val requestSender = RequestSender(WebClient.builder().build())

    private lateinit var username: String
    private lateinit var userId: String

    @Before
    fun createActiveTestUser() {
        this.username = "${UUID.randomUUID()}@test.com"
        val user = mapOf(
            "username" to this.username,
            "password" to "MySuperSecurePassword12345"
        )
        this.userId = requestSender
            .post(EndpointUtilities.RMW.Auth.NEW_USER_ENDPOINT, user)
            .map { response -> response.body?.findPath("data")?.textValue() ?: "" }
            .get()
    }

    @Test
    fun `Authenticating a user should succeed`() {
        requestSender.get(EndpointUtilities.RMW.Auth.activateUserEndpoint(this.userId))
        val user: Map<String, String> = mapOf(
            "username" to this.username,
            "password" to "MySuperSecurePassword12345"
        )
        requestSender.post(EndpointUtilities.RMW.Auth.AUTHENTICATE_USER_ENDPOINT, user)
            .map { response ->
                when {
                    response.statusCode != HttpStatus.OK ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.OK, response.statusCode))
                    response.body == null ->
                        fail(MessageUtilities.ERROR_EXPECTED_BODY)
                }
            }
    }

    @Test
    fun `Authenticating a user should reject invalid credentials`() {
        requestSender.get(EndpointUtilities.RMW.Auth.activateUserEndpoint(this.userId))
        val user: Map<String, String> = mapOf(
            "username" to this.username,
            "password" to "MySuperSecurePassword12345"
        )
        requestSender
            .post(EndpointUtilities.RMW.Auth.AUTHENTICATE_USER_ENDPOINT, mapOf(
                "username" to user.getOrDefault("username", ""),
                "password" to "incorrect_password"
            ))
            .map { response ->
                when {
                    response.statusCode != HttpStatus.UNAUTHORIZED ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.UNAUTHORIZED, response.statusCode))
                    response.body == null ->
                        fail(MessageUtilities.ERROR_EXPECTED_BODY)
                    response.body?.findPath("data")?.textValue() != "Failed to authenticate the user." ->
                        fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                }
            }
    }

    @Test
    fun `Authenticating a user should fail for inactive users`() {
        val user: Map<String, String> = mapOf(
            "username" to this.username,
            "password" to "MySuperSecurePassword12345"
        )
        requestSender.post(EndpointUtilities.RMW.Auth.AUTHENTICATE_USER_ENDPOINT, user)
            .map { response ->
                when {
                    response.statusCode != HttpStatus.FORBIDDEN ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.FORBIDDEN, response.statusCode))
                    response.body == null ->
                        fail(MessageUtilities.ERROR_EXPECTED_BODY)
                    response.body?.findPath("data")?.textValue() != "The user must be activated before it can be authenticated." ->
                        fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                }
            }
    }

    @Test
    fun `Authenticating a user should return a 404 response when a user is not found`() {
        requestSender
            .post(EndpointUtilities.RMW.Auth.AUTHENTICATE_USER_ENDPOINT, mapOf(
                "username" to "123@test.com",
                "password" to "MySuperSecurePassword12345"
            ))
            .map { response ->
                when {
                    response.statusCode != HttpStatus.NOT_FOUND ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.NOT_FOUND, response.statusCode))
                    response.body != null ->
                        fail(MessageUtilities.ERROR_UNEXPECTED_BODY)
                }
            }
    }

    @Test
    fun `Viewing a user by an authentication token should succeed`() {
        requestSender.get(EndpointUtilities.RMW.Auth.activateUserEndpoint(this.userId))
        val authenticationToken = requestSender
            .post(EndpointUtilities.RMW.Auth.AUTHENTICATE_USER_ENDPOINT, mapOf(
                "username" to this.username,
                "password" to "MySuperSecurePassword12345"
            ))
            .map { response -> response.body?.findPath("data")?.textValue() ?: "" }
            .get()
        requestSender
            .get(EndpointUtilities.RMW.Auth.VIEW_USER_BY_AUTH_TOKEN, authenticationToken)
            .map { response ->
                when {
                    response.statusCode != HttpStatus.OK ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.BAD_REQUEST, response.statusCode))
                    response.body == null ->
                        fail(MessageUtilities.ERROR_EXPECTED_BODY)
                    response.body?.findPath("id")?.textValue() != this.userId ->
                        fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                }
            }
    }

    @Test
    fun `Viewing a user by an authentication token should reject unauthorised entities`() {
        requestSender
            .get(EndpointUtilities.RMW.Auth.VIEW_USER_BY_AUTH_TOKEN)
            .map { response ->
                when {
                    response.statusCode != HttpStatus.UNAUTHORIZED ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.UNAUTHORIZED, response.statusCode))
                    response.body == null ->
                        fail(MessageUtilities.ERROR_EXPECTED_BODY)
                    response.body?.findPath("data")?.textValue() != "The request did not contain a valid authentication token" ->
                        fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                }
            }
    }
}