package oms.tester.rmw.auth

import junit.framework.TestCase.fail
import oms.tester.utility.EndpointUtilities
import oms.tester.utility.MessageUtilities
import oms.tester.utility.RequestSender
import oms.tester.utility.TestContextProvider
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.web.reactive.function.client.WebClient
import java.util.UUID

@RunWith(SpringRunner::class)
@ContextConfiguration(classes = [TestContextProvider::class])
@WebFluxTest
class UserIntegrationTest {

    private val requestSender = RequestSender(WebClient.builder().build())

    private lateinit var username: String
    private lateinit var userId: String
    private lateinit var authenticationToken: String

    @Before
    fun createActiveTestUser() {
        this.username = "${UUID.randomUUID()}@test.com"
        val user = mapOf(
            "username" to this.username,
            "password" to "MySuperSecurePassword12345"
        )
        this.userId = requestSender
            .post(EndpointUtilities.RMW.Auth.NEW_USER_ENDPOINT, user)
            .map { response -> response.body?.findPath("data")?.textValue() ?: "" }
            .get()
        requestSender.get(EndpointUtilities.RMW.Auth.activateUserEndpoint(this.userId))
        this.authenticationToken = requestSender
            .post(EndpointUtilities.RMW.Auth.AUTHENTICATE_USER_ENDPOINT, user)
            .map { response -> response.body?.findPath("data")?.textValue() ?: "" }
            .get()
    }

    @Test
    fun `Creating a user should succeed`() {
        requestSender.post(EndpointUtilities.RMW.Auth.NEW_USER_ENDPOINT, mapOf(
            "username" to "${UUID.randomUUID()}@test.com",
            "password" to "MySuperSecurePassword123"
        )).map { response ->
            when {
                response.statusCode != HttpStatus.CREATED ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.CREATED, response.statusCode))
                response.body == null ->
                    fail(MessageUtilities.ERROR_EXPECTED_BODY)
            }
        }
    }

    @Test
    fun `Creating a user should reject invalid requests`() {
        requestSender.post(EndpointUtilities.RMW.Auth.NEW_USER_ENDPOINT, mapOf(
            "username" to "not_valid"
        ), null).map { response ->
            when {
                response.statusCode != HttpStatus.BAD_REQUEST ->
                    fail(MessageUtilities.invalidStatus(HttpStatus.CREATED, response.statusCode))
                response.body == null ->
                    fail(MessageUtilities.ERROR_EXPECTED_BODY)
                else -> {
                    val faultMessages = response.body!!.findPath("data").toList()
                        .map { node -> node.findValue("message").textValue() }
                    when {
                        !faultMessages.contains("The mandatory password field was expected but was not provided.") ->
                            fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                        !faultMessages.contains("The provided email address was not valid.") ->
                            fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                    }
                }
            }
        }
    }

    @Test
    fun `Creating a user should reject existing users`() {
        val user = mapOf(
            "username" to this.username,
            "password" to "MySuperSecurePassword123"
        )
        requestSender.post(EndpointUtilities.RMW.Auth.NEW_USER_ENDPOINT, user)
        requestSender.post(EndpointUtilities.RMW.Auth.NEW_USER_ENDPOINT, user)
            .map { response ->
                when {
                    response.statusCode != HttpStatus.BAD_REQUEST ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.BAD_REQUEST, response.statusCode))
                    response.body == null ->
                        fail(MessageUtilities.ERROR_EXPECTED_BODY)
                    response.body!!.findPath("data").textValue() != "The provided email address is associated with an existing account." ->
                        fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                }
            }
    }

    @Test
    fun `Updating a user should succeed`() {
        requestSender
            .put(
                EndpointUtilities.RMW.Auth.updateUserEndpoint(this.userId),
                mapOf("password" to "MyPassword123456"),
                this.authenticationToken)
            .map { response ->
                when {
                    response.statusCode != HttpStatus.OK ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.OK, response.statusCode))
                    response.body != null ->
                        fail(MessageUtilities.ERROR_UNEXPECTED_BODY)
                }
            }
    }

    @Test
    fun `Updating a user should reject unauthorised entities`() {
        requestSender
            .put(
                EndpointUtilities.RMW.Auth.updateUserEndpoint(this.userId),
                mapOf("password" to "MyPassword123456"))
            .map { response ->
                when {
                    response.statusCode != HttpStatus.UNAUTHORIZED ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.UNAUTHORIZED, response.statusCode))
                    response.body == null ->
                        fail(MessageUtilities.ERROR_EXPECTED_BODY)
                    response.body?.findPath("data")?.textValue() != "The request did not contain a valid authentication token" ->
                        fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                }
            }
    }

    @Test
    fun `Updating a user should reject invalid requests`() {
        requestSender
            .put(
                EndpointUtilities.RMW.Auth.updateUserEndpoint(this.userId),
                mapOf("password" to "not_valid"),
                this.authenticationToken)
            .map { response ->
                when {
                    response.statusCode != HttpStatus.BAD_REQUEST ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.BAD_REQUEST, response.statusCode))
                    response.body == null ->
                        fail(MessageUtilities.ERROR_EXPECTED_BODY)
                    else -> {
                        val faultMessages = response.body!!.findPath("data").toList()
                            .map { node -> node.findValue("message").textValue() }
                        when {
                            !faultMessages.contains("A password must be at least 8 characters in length and have one letter and one number.") ->
                                fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                        }
                    }
                }
            }
    }

    @Test
    fun `Updating a user should reject usernames`() {
        requestSender
            .put(
                EndpointUtilities.RMW.Auth.updateUserEndpoint(this.userId),
                mapOf("username" to this.username),
                this.authenticationToken)
            .map { response ->
                when {
                    response.statusCode != HttpStatus.BAD_REQUEST ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.BAD_REQUEST, response.statusCode))
                    response.body == null ->
                        fail(MessageUtilities.ERROR_EXPECTED_BODY)
                    else -> {
                        val faultMessages = response.body!!.findPath("data").toList()
                            .map { node -> node.findValue("message").textValue() }
                        when {
                            !faultMessages.contains("An accounts username cannot be updated.") ->
                                fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                        }
                    }
                }
            }
    }

    @Test
    fun `Updating a user should return a 404 response when a user cannot be found`() {
        requestSender
            .put(
                EndpointUtilities.RMW.Auth.updateUserEndpoint("12345"),
                mapOf("password" to "MyPassword123456"),
                this.authenticationToken)
            .map { response ->
                when {
                    response.statusCode != HttpStatus.NOT_FOUND ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.BAD_REQUEST, response.statusCode))
                    response.body != null ->
                        fail(MessageUtilities.ERROR_UNEXPECTED_BODY)
                }
            }
    }

    @Test
    fun `Removing a user should succeed`() {
        requestSender
            .delete(
                EndpointUtilities.RMW.Auth.deleteUserEndpoint(this.userId),
                this.authenticationToken)
            .map { response ->
                when {
                    response.statusCode != HttpStatus.OK ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.OK, response.statusCode))
                    response.body != null ->
                        fail(MessageUtilities.ERROR_UNEXPECTED_BODY)
                }
            }
    }

    @Test
    fun `Removing a user should reject unauthorised users`() {
        requestSender
            .delete(EndpointUtilities.RMW.Auth.deleteUserEndpoint(this.userId))
            .map { response ->
                when {
                    response.statusCode != HttpStatus.UNAUTHORIZED ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.UNAUTHORIZED, response.statusCode))
                    response.body == null ->
                        fail(MessageUtilities.ERROR_EXPECTED_BODY)
                    response.body?.findPath("data")?.textValue() != "The request did not contain a valid authentication token" ->
                        fail(MessageUtilities.ERROR_MISSING_MESSAGES)
                }
            }
    }

    @Test
    fun `Removing a user should return a 404 response when a user cannot be found`() {
        requestSender
            .delete(
                EndpointUtilities.RMW.Auth.deleteUserEndpoint("12345"),
                this.authenticationToken)
            .map { response ->
                when {
                    response.statusCode != HttpStatus.NOT_FOUND ->
                        fail(MessageUtilities.invalidStatus(HttpStatus.BAD_REQUEST, response.statusCode))
                    response.body != null ->
                        fail(MessageUtilities.ERROR_UNEXPECTED_BODY)
                }
            }
    }
}