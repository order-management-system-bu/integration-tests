package oms.tester.utility

import org.springframework.http.HttpStatus

/**
 * Object is responsible for providing generic error messages
 *
 * @author Liam McGuire
 * @since 0.0.1
 */
object MessageUtilities {
    const val FAILED_TO_CREATE_TEST_ENTITIY: String =
        "Failed to create the test entity."

    const val ERROR_EXPECTED_BODY: String =
        "A JSON body was expected but not found in the response."
    const val ERROR_UNEXPECTED_BODY: String =
        "The response contained an unexpected JSON body."
    const val ERROR_MISSING_MESSAGES: String =
        "One or more expected messages where missing from the response."

    fun invalidStatus(expectedStatus: HttpStatus, status: HttpStatus): String =
        "A ${expectedStatus.value()} response was expected but a ${status.value()} was provided."
}