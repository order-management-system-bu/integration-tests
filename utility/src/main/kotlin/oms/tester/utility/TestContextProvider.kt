package oms.tester.utility

import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
open class TestContextProvider