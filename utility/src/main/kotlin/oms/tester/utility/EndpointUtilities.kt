package oms.tester.utility

/**
 * Object is responsible for providing endpoints to various
 * Order Management System applications
 *
 * @author Liam McGuire
 * @since 0.0.1
 */
object EndpointUtilities {
    private const val ROOT: String = "http://127.0.0.1:9000"

    object RMW {
        object Auth {
            const val NEW_USER_ENDPOINT: String = "$ROOT/oms/rmw/auth/user/"
            const val VIEW_USER_BY_AUTH_TOKEN: String = "$ROOT/oms/rmw/auth/user/view/"
            const val AUTHENTICATE_USER_ENDPOINT: String = "$ROOT/oms/rmw/auth/authenticate/"

            fun updateUserEndpoint(id: String?): String = "$ROOT/oms/rmw/auth/user/$id/update/"

            fun deleteUserEndpoint(id: String?): String = "$ROOT/oms/rmw/auth/user/$id/delete"

            fun activateUserEndpoint(id: String?): String = "$ROOT/oms/rmw/auth/activate/$id/"
        }

        object CRM {
            object Customer {
                const val NEW_CUSTOMER_ENDPOINT: String = "$ROOT/oms/rmw/crm/customer"

                fun updateCustomerEndpoint(id: String?): String = "$ROOT/oms/rmw/crm/customer/$id/update"

                fun deleteCustomerEndpoint(id: String?): String = "$ROOT/oms/rmw/crm/customer/$id/delete"

                fun viewCustomerEndpoint(id: String?): String = "$ROOT/oms/rmw/crm/customer/$id/view"
            }

            object Address {
                const val NEW_ADDRESS_ENDPOINT: String = "$ROOT/oms/rmw/crm/address"

                fun updateAddressEndpoint(id: String?): String = "$ROOT/oms/rmw/crm/address/$id/update"

                fun deleteAddressEndpoint(id: String?): String = "$ROOT/oms/rmw/crm/address/$id/delete"

                fun viewAddressEndpoint(id: String?): String = "$ROOT/oms/rmw/crm/address/$id/view"
            }
        }
    }
}