package oms.tester.utility

import com.fasterxml.jackson.databind.JsonNode
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import java.util.Optional

/**
 * Utility class is responsible for sending HTTP requests to
 * various endpoints in the Order Management System.
 *
 * @author Liam McGuire
 * @since 0.0.1
 */
class RequestSender(
    private val webClient: WebClient
) {
    /**
     * Function is responsible for sending a HTTP POST request to the
     * provided endpoint.
     *
     * @author Liam McGuire
     * @since 0.0.1
     */
    fun post(endpoint: String, payload: Map<String, String>, authenticationToken: String? = null): Optional<ResponseEntity<JsonNode>> = webClient
        .post().uri(endpoint)
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        .header("Authorization", authenticationToken)
        .body(BodyInserters.fromObject(payload))
        .exchange()
        .flatMap { response -> response.toEntity(JsonNode::class.java) }
        .blockOptional()

    /**
     * Function is responsible for sending a HTTP PUT request to the
     * provided endpoint.
     *
     * @author Liam McGuire
     * @since 0.0.1
     */
    fun put(endpoint: String, payload: Map<String, String>, authenticationToken: String? = null): Optional<ResponseEntity<JsonNode>> = webClient
        .put().uri(endpoint)
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        .header("Authorization", authenticationToken)
        .body(BodyInserters.fromObject(payload))
        .exchange()
        .flatMap { response -> response.toEntity(JsonNode::class.java) }
        .blockOptional()

    /**
     * Function is responsible for sending a HTTP GET request to the
     * provided endpoint.
     *
     * @author Liam McGuire
     * @since 0.0.1
     */
    fun get(endpoint: String, authenticationToken: String? = null): Optional<ResponseEntity<JsonNode>> = webClient
        .get().uri(endpoint)
        .header("Authorization", authenticationToken)
        .exchange()
        .flatMap { response -> response.toEntity(JsonNode::class.java) }
        .blockOptional()

    /**
     * Function is responsible for sending a HTTP DELETE request to the
     * provided endpoint.
     *
     * @author Liam McGuire
     * @since 0.0.1
     */
    fun delete(endpoint: String, authenticationToken: String? = null): Optional<ResponseEntity<JsonNode>> = webClient
        .delete().uri(endpoint)
        .header("Authorization", authenticationToken)
        .exchange()
        .flatMap { response -> response.toEntity(JsonNode::class.java) }
        .blockOptional()
}